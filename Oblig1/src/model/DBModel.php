<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel {        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  {  
	    if ($db) {
			$this->db = $db;
		}else {
            // Create PDO connection
			$this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'root', '');
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList() {
		$booklist = array();
        
		$query = $this->db->prepare('SELECT id, title, author, description FROM book');
		$query->execute();
		
		$rows = $query->fetchAll();
		
		foreach ($rows as $book) {
			$booklist[] = new Book($book['title'], $book['author'], $book['description'], $book['id']);
		}
		return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id) {
		if(!filter_var($id, FILTER_VALIDATE_INT)){
			return null;
		}
		
		$book = null;
		
		$query = $this->db->prepare('SELECT id, title, author, description FROM book WHERE id=:id');
		$query->bindParam(':id', $id, PDO::PARAM_INT);
		$query->execute();
		
		$book = $query->fetch(PDO::FETCH_ASSOC);
		
		if($book == FALSE)	{
			return null;
		}
		
        return $book = new Book(
			$book['title'],
			$book['author'],
			$book['description'],
			$book['id']	
		);
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */

    public function addBook($book) {	
	/*
		$query = $this->db->prepare('INSERT INTO book(id, title, author, description) VALUES(:id, :title, :author, :description)');
		$query->bindParam(':id', ($this->db->lastInsertId() + 1), PDO::PARAM_INT);
		$query->bindParam(':title', $book->title, PDO::PARAM_STR);
		$query->bindParam(':author', $book->author, PDO::PARAM_STR);
		$query->bindParam(':description', $book->description, PDO::PARAM_STR);
		$query->execute();
		*/
		try{
			$tempBook = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (?,?,?)");
			$tempBook->execute(array($book->title, $book->author, $book->description));
			$book->id = $this->db->lastInsertId();
		} catch(PDOException $ex){
			$view = new ErrorView();
			$view->create();
		}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book) {
		$query = $this->db->prepare('UPDATE book SET title = :title, author = :author, description = :description WHERE id = :id');
		$query->bindParam(':title', $book->title, PDO::PARAM_STR);
		$query->bindParam(':author', $book->author, PDO::PARAM_STR);
		$query->bindParam(':description', $book->description, PDO::PARAM_STR);
		$query->bindParam(':id', $book->id, PDO::PARAM_INT);
		$query->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id) {
		$query = $this->db->prepare('DELETE FROM book WHERE id =:id');
		
		$query->bindParam(':id', $id, PDO::PARAM_INT);
		$query->execute();
    }
}

?>